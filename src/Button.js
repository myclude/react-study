import PropTypes from "prop-types";

function Button({ text }) {
    return <button className="btn btn-outline-primary" >{text}</button>
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
}

export default Button;